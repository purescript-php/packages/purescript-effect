<?php

$exports["mkEffectFn1"] = function($fn) {
  return function($x) use (&$fn) {
    return $fn($x)();
  };
};

$exports["mkEffectFn2"] = function($fn) {
  return function($a, $b) use (&$fn) {
    return $fn($a)($b)();
  };
};

$exports["mkEffectFn3"] = function($fn) {
  return function($a, $b, $c) use (&$fn) {
    return $fn($a)($b)($c)();
  };
};

$exports["mkEffectFn4"] = function($fn) {
  return function($a, $b, $c, $d) use (&$fn) {
    return $fn($a)($b)($c)($d)();
  };
};

$exports["mkEffectFn5"] = function($fn) {
  return function($a, $b, $c, $d, $e) use (&$fn) {
    return $fn($a)($b)($c)($d)($e)();
  };
};

$exports["mkEffectFn6"] = function($fn) {
  return function($a, $b, $c, $d, $e, $f) use (&$fn) {
    return $fn($a)($b)($c)($d)($e)($f)();
  };
};

$exports["mkEffectFn7"] = function($fn) {
  return function($a, $b, $c, $d, $e, $f, $g) use (&$fn) {
    return $fn($a)($b)($c)($d)($e)($f)($g)();
  };
};

$exports["mkEffectFn8"] = function($fn) {
  return function($a, $b, $c, $d, $e, $f, $g, $h) use (&$fn) {
    return $fn($a)($b)($c)($d)($e)($f)($g)($h)();
  };
};

$exports["mkEffectFn9"] = function($fn) {
  return function($a, $b, $c, $d, $e, $f, $g, $h, $i) use (&$fn) {
    return $fn($a)($b)($c)($d)($e)($f)($g)($h)($i)();
  };
};

$exports["mkEffectFn10"] = function($fn) {
  return function($a, $b, $c, $d, $e, $f, $g, $h, $i, $j) use (&$fn) {
    return $fn($a)($b)($c)($d)($e)($f)($g)($h)($i)($j)();
  };
};

$exports["runEffectFn1"] = function ($fn) {
  return function($a) use (&$fn) {
    return function() use (&$fn, &$a) {
      return $fn($a);
    };
  };
};

$exports["runEffectFn2"] = function ($fn) {
  return function($a) use (&$fn) {
    return function($b) use (&$fn, &$a) {
      return function() use (&$fn, &$a, &$b) {
        return $fn($a, $b);
      };
    };
  };
};

$exports["runEffectFn3"] = function ($fn) {
  return function($a) use (&$fn) {
    return function($b) use (&$fn, &$a) {
      return function($c) use (&$fn, &$a, &$b) {
        return function() use (&$fn, &$a, &$b, &$c) {
          return $fn($a, $b, $c);
        };
      };
    };
  };
};

$exports["runEffectFn4"] = function ($fn) {
  return function($a) use (&$fn) {
    return function($b) use (&$fn, &$a) {
      return function($c) use (&$fn, &$a, &$b) {
        return function($d) use (&$fn, &$a, &$b, &$c) {
          return function() use (&$fn, &$a, &$b, &$c, &$d) {
            return $fn($a, $b, $c, $d);
          };
        };
      };
    };
  };
};

$exports["runEffectFn5"] = function ($fn) {
  return function($a) use (&$fn) {
    return function($b) use (&$fn, &$a) {
      return function($c) use (&$fn, &$a, &$b) {
        return function($d) use (&$fn, &$a, &$b, &$c) {
          return function($e) use (&$fn, &$a, &$b, &$c, &$d) {
            return function() use (&$fn, &$a, &$b, &$c, &$d, &$e) {
              return $fn($a, $b, $c, $d, $e);
            };
          };
        };
      };
    };
  };
};

$exports["runEffectFn6"] = function ($fn) {
  return function($a) use (&$fn) {
    return function($b) use (&$fn, &$a) {
      return function($c) use (&$fn, &$a, &$b) {
        return function($d) use (&$fn, &$a, &$b, &$c) {
          return function($e) use (&$fn, &$a, &$b, &$c, &$d) {
            return function($f) use (&$fn, &$a, &$b, &$c, &$d, &$e) {
              return function() use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f) {
                return $fn($a, $b, $c, $d, $e, $f);
              };
            };
          };
        };
      };
    };
  };
};

$exports["runEffectFn7"] = function ($fn) {
  return function($a) use (&$fn) {
    return function($b) use (&$fn, &$a) {
      return function($c) use (&$fn, &$a, &$b) {
        return function($d) use (&$fn, &$a, &$b, &$c) {
          return function($e) use (&$fn, &$a, &$b, &$c, &$d) {
            return function($f) use (&$fn, &$a, &$b, &$c, &$d, &$e) {
              return function($g) use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f) {
                return function() use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f, &$g) {
                  return $fn($a, $b, $c, $d, $e, $f, $g);
                };
              };
            };
          };
        };
      };
    };
  };
};

$exports["runEffectFn8"] = function ($fn) {
  return function($a) use (&$fn) {
    return function($b) use (&$fn, &$a) {
      return function($c) use (&$fn, &$a, &$b) {
        return function($d) use (&$fn, &$a, &$b, &$c) {
          return function($e) use (&$fn, &$a, &$b, &$c, &$d) {
            return function($f) use (&$fn, &$a, &$b, &$c, &$d, &$e) {
              return function($g) use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f) {
                return function($h) use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f, &$g) {
                  return function() use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h) {
                    return $fn($a, $b, $c, $d, $e, $f, $g, $h);
                  };
                };
              };
            };
          };
        };
      };
    };
  };
};

$exports["runEffectFn9"] = function ($fn) {
  return function($a) use (&$fn) {
    return function($b) use (&$fn, &$a) {
      return function($c) use (&$fn, &$a, &$b) {
        return function($d) use (&$fn, &$a, &$b, &$c) {
          return function($e) use (&$fn, &$a, &$b, &$c, &$d) {
            return function($f) use (&$fn, &$a, &$b, &$c, &$d, &$e) {
              return function($g) use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f) {
                return function($h) use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f, &$g) {
                  return function($i) use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h) {
                    return function() use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$i) {
                      return $fn($a, $b, $c, $d, $e, $f, $g, $h, $i);
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  };
};

$exports["runEffectFn10"] = function ($fn) {
  return function($a) use (&$fn) {
    return function($b) use (&$fn, &$a) {
      return function($c) use (&$fn, &$a, &$b) {
        return function($d) use (&$fn, &$a, &$b, &$c) {
          return function($e) use (&$fn, &$a, &$b, &$c, &$d) {
            return function($f) use (&$fn, &$a, &$b, &$c, &$d, &$e) {
              return function($g) use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f) {
                return function($h) use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f, &$g) {
                  return function($i) use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h) {
                    return function($j) use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$i) {
                      return function() use (&$fn, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$i, &$j) {
                        return $fn($a, $b, $c, $d, $e, $f, $g, $h, $i, $j);
                      };
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  };
};
